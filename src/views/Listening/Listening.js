import React, {Component} from 'react';
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Fade,
  Form,
  FormGroup,
  Input,
  Label,
  Row
} from 'reactstrap';

class Sentence {
  constructor() {
    this.content = "";
    this.sound_url = "";
    this.ipa = "";
    this.audio = null;
    this.fade = true;
    this.is_saved = false;
  }
}

const encode_keyword = function(keyword) {
  return keyword.split(' ').join('+');
};

const split_chunk = (arr, chunk_size) => {
  let results = [];
  while(arr.length) {
    results.push(arr.splice(0, chunk_size));
  }
  return results;
};

const get_ipa_async2 = (sentences) => {
  let input = sentences.join(' | ');
  let input_array = input.split(' ');
  let input_chunks = split_chunk(input_array, 450);
  const url = 'https://cors-anywhere.herokuapp.com/http://upodn.com/phon.php';
  let fetch_list = [];

  input_chunks.forEach(chunk => {
    let formData = new FormData();
    formData.append('intext', chunk.join(' '));
    formData.append('ipa', 0);
    formData.append('stress', 'on');

    fetch_list.push(
      fetch(url, {
        'method': 'POST',
        headers: {
          'origin': 'http://upodn.com/phon.php'
        },
        body: formData
      }).then(response => {
        return response.text();
      }).then(data => {
        let parser = new DOMParser();
        let html = parser.parseFromString(data, 'text/html');
        let tables = html.getElementsByTagName('table');
        let table = tables[0];
        let second_column = table.getElementsByTagName('td')[1];
        return second_column.textContent.replace(/\s+/g, " ").trim();
      }).catch(error => {
        console.log(error);
      })
    );
  });

  return Promise.all(fetch_list).then(all => {
    let text = all.join(' ');
    return text.split(' | ')
  });
};


const get_ipa_async = (sentences) => {
  const input = sentences.join('|');
  let formData = new FormData();
  formData.append('text_to_transcribe', input);
  formData.append('submit', 'Show transcription');
  formData.append('output_dialect', 'br');
  formData.append('output_style', 'only_tr');

  const BASE_URL = 'https://cors-anywhere.herokuapp.com/https://tophonetics.com/';
  return fetch(BASE_URL, {
    method: 'POST',
    headers: {
      'origin': 'https://tophonetics.com'
    },
    body: formData
  }).then(response => {
    return response.text();
  }).then(data => {
    let parser = new DOMParser();
    let html = parser.parseFromString(data, 'text/html');
    let outputs = html.getElementById('transcr_output').getElementsByTagName('span');

    let words = [];
    for (let i = 0; i < outputs.length; i++) {
      let word = outputs[i].textContent.replace(/\s+/g, " ").trim();
      words.push(word);
    }
    words = words.join(' ');
    return words.split('|');
  }).catch(error => {
    console.log(error);
  });
};


const get_sentences_async = (keyword) => {
  const BASE_URL = 'https://cors-anywhere.herokuapp.com/https://www.ldoceonline.com/search/english/direct/?q=';
  return fetch(BASE_URL + encode_keyword(keyword), {
    headers: {
      'origin': 'https://www.ldoceonline.com'
    }
  }).then(response => {
    return response.text();
  }).then(data => {
    let parser = new DOMParser();
    let html = parser.parseFromString(data, 'text/html');
    let examples = html.getElementsByClassName('EXAMPLE');

    // invalid keyword
    if (examples.length === 0) {
      return [[], html];
    }

    // valid keyword
    let sentences = [];
    for (let i = 0; i < examples.length; i++) {
      let example = examples[i];
      // get sound url
      let speaker = example.getElementsByClassName('speaker')[0];

      // if this example doesn't contain speakers
      if (!speaker){
        continue;
      }

      let sound_url = speaker.getAttribute('data-src-mp3');
      // get content
      let content = example.textContent.replace(/\s+/g, " ").trim();

      // map to sentence
      let sentence = new Sentence();
      sentence.content = content;
      sentence.sound_url = sound_url;
      sentence.fade = true;
      sentence.audio = new Audio(sound_url);

      // check is_saved
      let bookmarks = JSON.parse(localStorage.getItem('bookmarks'));
      if (bookmarks.some(t => t['content'] === content)){
        sentence.is_saved = true;
      }
      sentences.push(sentence);
    }
    return [sentences, html];
  }).catch(error => {
    console.log(error);
    return null;
  });
};

class Listening extends Component {

  constructor(props) {
    super(props);
    this.init_state = {
      'keyword': '',
      'max_words': 1000,
      'sentences': [],
      'loading': false,
      'in_bookmark': false,
      'suggests':  []
    };
    this.state = this.init_state;
  }

  reset = () => {
    this.setState(this.init_state);
  };

  componentDidMount() {
    this.reset();
    let bookmarks = JSON.parse(localStorage.getItem('bookmarks'));
    if (!bookmarks) {
      localStorage.setItem('bookmarks', JSON.stringify([]));
    }
  }

  on_click_search = async () => {
    let keyword = this.state.keyword;

    // let max_words = this.state.max_words;
    this.setState({'loading': true, 'in_bookmark': false, 'suggests': []});
    let result = await get_sentences_async(keyword);
    let sentences = result[0];
    let html = result[1];

    if (sentences.length === 0) {
      let didyoumean = html;
      didyoumean = didyoumean.getElementsByClassName('didyoumean');
      if (didyoumean.length === 0) {
        // did you mean not found
        this.setState({'sentences': sentences, 'loading': false});
        return;
      }
      didyoumean = didyoumean[0];
      let suggests = didyoumean.getElementsByTagName('a');
      let texts = [];
      for (let i = 0; i < suggests.length; i++) {
        let text = suggests[i].textContent.replace(/\s+/g, " ").trim();
        texts.push(text);
      }
      this.setState({'sentences': sentences, 'loading': false, 'suggests': texts});
    } else {
      this.setState({'sentences': sentences, 'loading': false});
      let contents = sentences.map(t => t.content);
      let ipas = await get_ipa_async(contents);
      sentences = this.state.sentences.slice();
      for (let i = 0; i < sentences.length; i++) {
        sentences[i].ipa = ipas[i];
      }
      this.setState({'sentences': sentences});
    }
  };

  toggle = (id) => {
    let sentences = this.state.sentences.map((t, i) => (i === id ? {...t, fade: !t.fade}: t));
    this.setState({'sentences': sentences});
  };

  play_sound = (id) => {
    this.state.sentences.forEach(sentence => {
      sentence.audio.pause();
      sentence.audio.currentTime = 0;
    });
    const sentence = this.state.sentences[id];
    sentence.audio.play();
  };

  bookmark = (id) => {
    let bookmark = this.state.sentences[id];

    // change state to is_saved
    // let sentences = this.state.sentences.slice();
    // sentences[id]['is_saved'] = true;
    let sentences = this.state.sentences.map((t, i) => (i === id ? {...t, is_saved: true}: t));
    this.setState({sentences});

    // save to local storage
    bookmark = {...bookmark};
    delete bookmark['audio'];
    delete bookmark['fade'];

    let bookmarks = JSON.parse(localStorage.getItem('bookmarks'));

    // if this bookmark hasn't existed

    let focus = bookmarks.some(t => t['content'] === bookmark['content']);
    if (!focus) {
      bookmarks.unshift(bookmark);
    }
    localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
  };

  open_bookmark = () => {
    let bookmarks = JSON.parse(localStorage.getItem('bookmarks'));
    this.reset();
    let sentences = [];
    for (let i = 0; i < bookmarks.length; i++) {
      let sentence = new Sentence();
      sentence.content = bookmarks[i]['content'];
      sentence.sound_url = bookmarks[i]['sound_url'];
      sentence.ipa = bookmarks[i]['ipa'];
      sentence.fade = false;
      sentence.is_saved = true;
      sentence.audio = new Audio(sentence.sound_url);
      sentences.push(sentence);
    }
    let in_bookmark = true;
    this.setState({sentences, in_bookmark});
  };
  
  toggle_all = (type) => {
    if (type === 'show') {
      let sentences = this.state.sentences.map(t => ({...t, fade: false}));
      this.setState({sentences});
	  }
	  if (type === 'hide') {
      let sentences = this.state.sentences.map(t => ({...t, fade: true}));
      this.setState({sentences});
	  }
  };

  remove_bookmark = (id) => {
    let sentence = this.state.sentences[id];
    let bookmarks = JSON.parse(localStorage.getItem('bookmarks'));
    bookmarks = bookmarks.filter(t => t['content'] !== sentence.content);
    localStorage.setItem('bookmarks', JSON.stringify(bookmarks));

    if (this.state.in_bookmark) {
      // if in bookmark then remove from list
      let sentences = this.state.sentences.slice();
      sentences.splice(id, 1);
      this.setState({sentences});
    } else {
      // change state
      let sentences = this.state.sentences.map((t, i) => (i === id ? {...t, is_saved: false}: t));
      this.setState({sentences});
    }
  };

  render() {
    return (
      <div className="animated fadeIn">
            <Card>
              <CardHeader><strong>Search on <a href="https://www.ldoceonline.com">ldoceonline</a></strong></CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <FormGroup className="pr-1">
                    <Label htmlFor="exampleInputName2" className="pr-1">Search </Label>
                    <Input type="text" id="input-normal"
                         value={this.state.keyword}
                         onChange={e => this.setState({'keyword': e.target.value})}
                         name="input-normal"
                         placeholder="e.g. he, she, cat"
                         onKeyPress={event => {
                           if (event.key === "Enter") {
                             event.preventDefault();
                             this.on_click_search();
                           }
                         }}
                    />
                  </FormGroup>
                  {
                    this.state.suggests.map((t, id) =>
                      <Button key = {id} className='btn-pill btn-sm' color="warning" style={{marginLeft: 4, marginTop: 5}} onClick={async () => {
                        await this.setState({'keyword': t});
                        this.on_click_search();
                      }}>{t}</Button>)
                  }
                  {/*<FormGroup className="pr-1">*/}
                    {/*<Label htmlFor="exampleInputEmail2" className="pr-1">Maximum number of words </Label>*/}
                    {/*<Input id="input-normal"*/}
                           {/*type="number"*/}
                           {/*onChange={e => this.setState({'max_words': e.target.value})}*/}
                           {/*value={this.state.max_words}*/}
                           {/*name="input-normal"*/}
                           {/*placeholder="e.g. 10, 15, 20"*/}
                           {/*onKeyPress={event => {*/}
                             {/*if (event.key === "Enter") {*/}
                               {/*this.on_click_search();*/}
                             {/*}*/}
                           {/*}}*/}
                    {/*/>*/}
                  {/*</FormGroup>*/}
                </Form>
              </CardBody>
              <CardFooter>
                <Button className='btn-pill btn-sm' onClick={this.on_click_search} color="primary" style={{marginLeft: 4, marginTop: 5}}>
                  {this.state.loading ? <i className={"fa fa-circle-o-notch fa-spin"}></i> : <i className={"fa fa-search"}></i>} Search
                </Button>
                <Button className='btn-pill btn-sm' onClick={this.reset} color="secondary" style={{marginLeft: 4, marginTop: 5}}>
                  <i className="fa fa-ban"></i> Reset
                </Button>
				        <Button className='btn-pill btn-sm' onClick={this.open_bookmark} color="danger" style={{marginLeft: 4, marginTop: 5}}>
                  <i className="fa fa-bookmark-o"></i> Bookmark
                </Button>
				        <Button className='btn-pill btn-sm' onClick={() => this.toggle_all('show')} color="dark" style={{marginLeft: 4, marginTop: 5}}>
                  <i className="fa fa-toggle-on"></i> Show
                </Button>
				        <Button className='btn-pill btn-sm' onClick={() => this.toggle_all('hide')} color="dark" style={{marginLeft: 4, marginTop: 5}}>
                  <i className="fa fa-toggle-on"></i> Hide
                </Button>
              </CardFooter>
            </Card>

        {/*Card*/}
        <Row>
          {
            this.state.sentences.map((t, id) =>
                <Col sm={12} md={6} key={id}>
                  <Card>
                    <CardBody>
                      <Fade in={!t.fade}>
                        <strong><p>{t.content}</p></strong>
                        {t.ipa ? <p>{t.ipa}</p>: <p style={{'whiteSpace': 'pre'}}>{" ".repeat(t.content.length)}</p>}
                      </Fade>
                    </CardBody>
                    <CardFooter>
                      <Button className='btn-pill btn-sm' color="primary" id="speak" onClick={() => this.play_sound(id)}><i className="icon-earphones"></i> Speak</Button>
                      <Button className='btn-pill btn-sm' color="dark" onClick={() => this.toggle(id)} id="toggleFade1" style={{marginLeft: 4}}><i className="fa fa-toggle-on"></i> Toggle</Button>
                      {
                        !t.is_saved ?
                        <Button className='btn-pill btn-sm' color="danger" onClick={() => this.bookmark(id)} id="toggleFade1" style={{marginLeft: 4}}><i className="fa fa-save"></i> Save</Button>
                        : <Button className='btn-pill btn-sm' color="secondary" onClick={() => this.remove_bookmark(id)} id="toggleFade1" style={{marginLeft: 4}}><i className="fa fa-remove"></i> Remove</Button>
                      }
                    </CardFooter>
                  </Card>
                </Col>
            )
          }
        </Row>

      </div>
    )
  }
}

export default Listening;
